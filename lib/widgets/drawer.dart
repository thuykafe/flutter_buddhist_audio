import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
        child: new ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 2,
                child: new Icon(
                  Icons.account_circle,
                  size: 55.0,
                ),
              ),
              new Expanded(
                flex: 1,
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      'Hello World',
                      style: new TextStyle(
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    new Text(
                      'This is the email display text',
                      style: new TextStyle(fontSize: 16.0, color: Colors.white),
                    ),
                  ],
                ),
              )
            ],
          ),
          decoration: new BoxDecoration(color: Colors.orange),
        ),
        ListTile(
          title: new Text('Item 1'),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
            title: new Text('Item 2'),
            onTap: () {
              Navigator.pop(context);
            })
      ],
    ));
  }
}
